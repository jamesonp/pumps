@ECHO OFF

echo Making Directory
echo .
mkdir "C:\Pump Calcs\"
echo .
echo Copying Files
echo .
xcopy /y /e * "C:\Pump Calcs\"
echo .
echo Install Complete
echo installed to: C:\Pump Calcs
echo .
pause